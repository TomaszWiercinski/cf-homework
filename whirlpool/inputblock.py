from whirlpool.poly import Polynomial
import numpy as np
from numpy.polynomial import Polynomial as P

fill = ['B3', 'C5', '44', '97', '42', '70', '9D', '88', '1B', '6A', 'CE', '10', '13', 'A8', '5F', '57',
        '8F', '0C', '24', 'F1', '9F', 'E9', 'A5', 'CD', 'D1', 'DC', 'D2', '6E', '16', 'A7', 'BB', 'E5']
fill = [int(hex, 16) for hex in fill]


def fill_block(block, length=16):
	remainder = len(block) % length
	fill_num = (length - remainder) if remainder > 0 or len(block) == 0 else 0
	return np.array(list(block) + fill[:fill_num])


def text_to_blocks(w, irr_poly_int):
	h = np.array([P([0])] * 16).reshape((4, 4))

	w_int = np.array([ord(letter) for letter in w])
	w_int = fill_block(w_int)

	m_blocks = []
	for block_i in range(int(len(w_int) / 16)):
		w_poly = []
		for w_i in w_int[block_i * 16:(block_i + 1) * 16]:
			w_poly.append(Polynomial(w_i, irr_poly_int))
		m_blocks.append(np.array(w_poly).reshape((4, 4)))
	return np.array(m_blocks)


if __name__ == '__main__':
	w = 'qwertyuiopasdfgh'
	m_blocks = text_to_blocks(w, int('15F', 16))

	print(f'Word: {w}\nNum blocks: {len(m_blocks)}')
	for block in m_blocks:
		print(block)

	w = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890!@#$%^&*-_=+([{<)]}>'
	m_blocks = text_to_blocks(w, int('15F', 16))

	print(f'\nWord: {w}\nNum blocks: {len(m_blocks)}')
	for block in m_blocks:
		print(block)
