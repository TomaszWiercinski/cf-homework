from whirlpool.poly import Polynomial
from whirlpool.inputblock import text_to_blocks
import numpy as np


def S(poly):
	irr_value = poly.irr_value
	return Polynomial(int('99', 16), irr_value) * ~poly + Polynomial(int('D5', 16), irr_value)


def SB(k, m):
	return np.array([[S(k_val) for k_val in k_row] for k_row in k]), \
	       np.array([[S(m_val) for m_val in m_row] for m_row in m])


def SC(k, m):
	k_ = np.zeros((4, 4), dtype=Polynomial)
	m_ = np.zeros((4, 4), dtype=Polynomial)
	for i in range(4):
		for j in range(4):
			k_[i, j] = k[i, (j + i) % 4]
			m_[i, j] = m[i, (j + i) % 4]
	return k_, m_


def MR(k, m):
	T = [1, 3, 5, 2, 2, 1, 3, 5, 5, 2, 1, 3, 3, 5, 2, 1]
	irr = k[0, 0].irr_value
	T = np.array([Polynomial(n, irr) for n in T]).reshape((4, 4))

	return np.matmul(T, k), np.matmul(T, m)


def AR(k, m, round):
	r = [['B4', 'DB', 'F5', 'F0'], ['9D', 'D2', 'ED', '40'], ['8B', '51', '95', '1C'], ['59', 'A8', 'E2', 'DF'], ['1A', 'D5', '04', '42'], ['11', '49', '26', '34']]
	r = r[round]
	irr = k[0, 0].irr_value
	r = np.array([Polynomial(int(n, 16), irr) for n in r])

	k[0, :] += r
	m += k
	return k, m


def WHIRLPOOL(w, irr):
	a_blocks = text_to_blocks(w, irr)
	h = np.array([Polynomial('0', '15F')] * 16).reshape((4, 4))

	for i, a in enumerate(a_blocks):
		# print(f'Processing block {i+1}/{len(a_blocks)}...')
		k, m = h, a + h
		for round in range(6):
			k, m = SB(k, m)
			k, m = SC(k, m)
			k, m = MR(k, m)
			k, m = AR(k, m, round)
		h = (h + m) + a

	return h.reshape(16)


def main():
	w = 'qe'
	irr = int('15F', 16)
	WHIRLPOOL(w, irr)


if __name__ == "__main__":
	main()