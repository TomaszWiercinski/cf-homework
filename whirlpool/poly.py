import numpy as np
from math import fabs


def extended_gcd(aa, bb):
	lastremainder, remainder = abs(aa), abs(bb)
	x, lastx = Polynomial(0, bb.irr_value), Polynomial(1, bb.irr_value)
	y, lasty = Polynomial(1, bb.irr_value), Polynomial(0, bb.irr_value)
	while remainder.value:
		lastremainder, (quotient, remainder) = remainder, divmod(lastremainder, remainder)
		x, lastx = lastx - quotient * x, x
		y, lasty = lasty - quotient * y, y
	return lastremainder, lastx, lasty


def modinv(a, m):
	g, x, y = extended_gcd(a, m)
	if g.value != 1:
		pass  # raise ValueError
	return x % m


def poly_div(p1, p2):
	def degree(poly):
		while poly and poly[-1] == 0:
			poly.pop()
		return len(poly)

	p1, p2 = [*reversed(p1)], [*reversed(p2)]

	p2_degree = degree(p2)
	p1_degree = degree(p1)

	if p2_degree <= 0:
		raise ZeroDivisionError

	if p1_degree >= p2_degree:
		q = [0] * p1_degree
		while p1_degree >= p2_degree:
			d = [0] * (p1_degree - p2_degree) + p2
			mult = q[p1_degree - p2_degree] = p1[-1] / float(d[-1])
			d = [coeff * mult for coeff in d]
			p1 = [fabs(p1_c - p2_c) for p1_c, p2_c in zip(p1, d)]
			p1_degree = degree(p1)
		r = p1
	else:
		q = [0]
		r = p1

	return np.array([*reversed(q)]), np.array([*reversed(r)])


class Polynomial:
	def __init__(self, a, irr_poly):
		if isinstance(a, str):
			self.value = int(a, 16)
			self.irr_value = int(irr_poly, 16)
			self.array = Polynomial.__dec_to_bin(self.value)
			self.irr_array = Polynomial.__dec_to_bin(self.irr_value)
		elif isinstance(a, (list, tuple, np.ndarray)):
			self.array = a
			self.irr_array = irr_poly
			self.value = int(np.polyval(a, 2))
			self.irr_value = int(np.polyval(irr_poly, 2))
		else:
			self.value = a
			self.irr_value = irr_poly
			self.array = Polynomial.__dec_to_bin(self.value)
			self.irr_array = Polynomial.__dec_to_bin(self.irr_value)

	def __add__(self, other):
		return Polynomial(self.value ^ other.value, self.irr_value)

	def __sub__(self, other):
		return Polynomial(self.value ^ other.value, self.irr_value)

	def __mul__(self, other):
		arr_mult = np.polymul(self.array, other.array)
		arr_mult = np.mod(arr_mult, 2)
		_, arr_mult = np.polydiv(arr_mult, self.irr_array)
		arr_mult = np.mod(arr_mult, 2)
		arr_mult = np.abs(arr_mult)
		return Polynomial(arr_mult, self.irr_array)

	def __eq__(self, other):
		return self.value == other.value

	def __str__(self):
		return f'{self.value:X}'

	__repr__ = __str__

	def __invert__(self):
		return modinv(self, Polynomial(self.irr_array, self.irr_array))

	def __divmod__(self, other):
		# arr_result, arr_remainder = np.polydiv(self.array, other.array)
		# arr_remainder = np.abs(arr_remainder)
		arr_result, arr_remainder = poly_div(self.array, other.array)
		return Polynomial(arr_result, self.irr_array), Polynomial(arr_remainder, self.irr_array)

	def __mod__(self, other):
		_, arr_remainder = poly_div(self.array, other.array)
		return Polynomial(arr_remainder, self.irr_array)
		if len(self.array) >= len(other.array):
			_, arr_remainder = np.polydiv(self.array, other.array)
			arr_remainder = np.abs(arr_remainder)
			return Polynomial(arr_remainder, self.irr_array)
		else:
			return self

	def __abs__(self):
		return Polynomial(np.abs(self.array), np.abs(self.irr_array))

	@staticmethod
	def __dec_to_bin(n):
		if n == 0:
			return [0]

		w_poly_num = []
		while n > 0:
			w_poly_num.append(n % 2)
			n -= w_poly_num[-1]
			n /= 2
		w_poly_num.reverse()
		return w_poly_num


if __name__ == "__main__":
	assert (Polynomial(4, 11) * Polynomial(4, 11)).value == 6
	assert (Polynomial(3, 11) * Polynomial(3, 11)).value == 5

	a = Polynomial('99', '15F') * Polynomial('1', '15F')
	assert (a + Polynomial('D5', '15F')).value == 76

	b = Polynomial('99', '15F') * ~Polynomial('2', '15F') + Polynomial('D5', '15F')
	print(~Polynomial('2', '15F') * Polynomial('2', '15F'))
	assert b.value == int('36', 16)

	c = Polynomial('99', '15F') * ~Polynomial('3', '15F') + Polynomial('D5', '15F')
	assert c.value == int('A2', 16)

	d = Polynomial('99', '15F') * ~Polynomial('4', '15F') + Polynomial('D5', '15F')
	print(~Polynomial('4', '15F') * Polynomial('4', '15F'))
	assert d.value == int('0B', 16)

	e = Polynomial('99', '15F') * ~Polynomial('0C', '15F') + Polynomial('D5', '15F')
	print(~Polynomial('C', '15F') * Polynomial('0C', '15F'))
	assert e.value == int('9F', 16)
