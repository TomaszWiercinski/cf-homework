from whirlpool import WHIRLPOOL, Polynomial
import numpy as np

irr = int('15F', 16)
words = ['', 'AbCxYz', '1234567890', 'Ala ma kota, kot ma ale.', 'Ty, ktory wchodzisz, zegnaj sie z nadzieja.', 'Litwo, Ojczyzno moja! ty jestes jak zdrowie;']
expected = ['[67 84 E8 9E 25 25 79 78 82 EA 3A 6D D 74 90 72]', '[E6 3E 1D 29 FB 9E 1F 24 8 B5 6D 1C FF F8 90 BD]', '[A7 6B 58 B5 FA 70 2E 85 18 C3 35 5F E2 C0 64 CF]', '[38 F4 A0 B 65 86 A7 1D DB 2A D4 25 A4 84 38 AC]', '[67 AF C7 C 78 6C 16 D2 21 8F 88 7E 19 CA 34 BC]', '[55 C8 9 49 AD 7D E6 FA DC 94 9A 86 57 2C 6 A2]']

for w, e in zip(words, expected):
    print(f'Input:\t\t"{w}"')
    print(f'Expected:\t{e}')
    print(f'Actual:\t\t{WHIRLPOOL(w, irr)}\n')
